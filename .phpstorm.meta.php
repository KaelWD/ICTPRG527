<?php
namespace PHPSTORM_META {

    $classMap = [
        'App\Support\Database' => \App\Support\Database::class,
        'App\Repository\CustomersRepository' => \App\Repository\CustomersRepository::class,
        'App\Repository\OrdersRepository' => \App\Repository\OrdersRepository::class,
        'App\Repository\ProductsRepository' => \App\Repository\ProductsRepository::class,
    ];

    override(\App\Support\Container\Container::make(0), map($classMap));
}
