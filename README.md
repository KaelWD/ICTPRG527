# ICTPRG527 - Apply intermediate object-oriented language skills

## Development setup instructions
 - Install PHP ≥7.1 and MariaDB ≥10.2
 - Clone the repository:

```bash
git clone https://gitlab.com/KaelWD/ICTPRG527
```
 - Create a database for the project using the `utf8mb4_unicode_ci` charset
 - Run `sql/create_tables.sql` against the database
 - Copy `config.example.php` to `config.php` and change the values to match the database created in step 3
 - Run `php -S localhost:80 -t public` to start the local development server
