<?php

namespace App;

use App\Database\ModelInterface;

class Customer implements ModelInterface
{
    public $id;
    public $name;
    public $address;
    public $phone;
}
