<?php

namespace App;

use App\Database\ModelInterface;

class Order implements ModelInterface
{
    public $id;
    public $customer_id;
    public $cost;

    public function costString(int $cost = null): string
    {
        $cost = $cost ?? $this->cost;
        return '$' . number_format($cost / 100, 2);
    }
}
