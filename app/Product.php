<?php

namespace App;

use App\Database\ModelInterface;

class Product implements ModelInterface
{
    public $id;
    public $name;
    public $description;
    public $price;
    public $stock;

    public function priceString(int $price = null): string
    {
        $price = $price ?? $this->price;
        return '$' . number_format($price / 100, 2);
    }
}
