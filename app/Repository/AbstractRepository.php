<?php

namespace App\Repository;

use PDO;
use ReflectionClass;
use ReflectionProperty;
use App\Support\Database;
use App\Database\ModelInterface as Model;

abstract class AbstractRepository implements RepositoryInterface
{
    protected $db;

    protected $table;

    protected $modelClass;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function find($id): Model
    {
        return $this->db
            ->query("SELECT * FROM `$this->table` WHERE id = ? LIMIT 1", $id)
            ->fetchObject($this->modelClass);
    }

    public function where($key, $value): array
    {
        return $this->db
            ->query("SELECT * FROM `$this->table` WHERE ? = ?", $key, $value)
            ->fetchObject($this->modelClass);
    }

    public function countAll(): int
    {
        return $this->db
            ->query("SELECT COUNT(*) AS count FROM `$this->table`")
            ->fetch(PDO::FETCH_OBJ)->count;
    }

    /** @return Model[] */
    public function all(): array
    {
        return $this->db
            ->query("SELECT * FROM `$this->table`")
            ->fetchAll(PDO::FETCH_CLASS, $this->modelClass);
    }

    public function save(Model $model): Model
    {
        $class = new ReflectionClass($model);
        $properties = array_map(
            function (ReflectionProperty $property) use ($model) {
                $value = $property->getValue($model);

                if (is_null($value)) return null;

                return [
                    'name'  => $property->getName(),
                    'value' => $property->getValue($model),
                ];
            },
            $class->getProperties(ReflectionProperty::IS_PUBLIC)
        );

        $propertyNames = array_column($properties, 'name');
        $propertyValues = array_column($properties, 'value');

        if ($model->id) {
            $placeholders = array_map(function ($name) {
                return "$name = ?";
            }, $propertyNames);

            $this->db->query(
                sprintf(
                // No injection on $model->id because we get $model from
                // the database before updating it
                    "UPDATE `$this->table` SET %s WHERE id = $model->id LIMIT 1",
                    implode(',', $placeholders)
                ),
                ...$propertyValues
            );

            return $this->find($model->id);
        } else {
            // Do not insert an empty ID
            if (($key = array_search('id', $propertyNames)) !== false) {
                unset($propertyNames[$key], $propertyValues[$key]);
            }

            $this->db->query(
                sprintf(
                    "INSERT INTO `$this->table` (%s) VALUES (%s)",
                    implode(',', $propertyNames),
                    implode(',', array_fill(0, count($propertyValues), '?'))
                ),
                ...$propertyValues
            );

            return $this->find($this->db->pdo->lastInsertId());
        }
    }
    
    public function delete(Model $model): bool
    {
        return $this->db->query(
            "DELETE FROM `$this->table` WHERE id = ? LIMIT 1",
            $model->id
        )->rowCount();
    }
}
