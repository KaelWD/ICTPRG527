<?php

namespace App\Repository;

use PDO;
use App\Customer;

class CustomersRepository extends AbstractRepository
{
    protected $table = 'customers';
    protected $modelClass = Customer::class;

    public function allWithOrdersCount(): array
    {
        return $this->db->query('
            SELECT customers.*, COUNT(orders.id) AS ordersCount FROM customers
            LEFT JOIN orders
            ON customers.id = orders.customer_id
            GROUP BY id
        ')->fetchAll(PDO::FETCH_CLASS, $this->modelClass);
    }
}
