<?php

namespace App\Repository;

use App\Product;
use PDO;
use App\Order;
use App\Database\ModelInterface as Model;

class OrdersRepository extends AbstractRepository
{
    protected $table      = 'orders';
    protected $modelClass = Order::class;

    /** @return Model[] */
    public function all(): array
    {
        return $this->db->query("
            SELECT orders.id,
                   orders.customer_id,
                   customers.name AS customer_name,
                   SUM(order_product.quantity) AS itemsCount,
                   SUM(order_product.quantity * products.price) AS cost
            FROM   orders
                   LEFT JOIN order_product
                          ON orders.id = order_product.order_id
                   LEFT JOIN products
                          ON products.id = order_product.product_id
                   LEFT JOIN customers
                          ON customers.id = orders.customer_id
            GROUP BY id
        ")->fetchAll(PDO::FETCH_CLASS, $this->modelClass);
    }

    public function whereCustomerID($id): array
    {
        return $this->db->query("
            SELECT orders.id,
                   SUM(order_product.quantity) AS itemsCount,
                   SUM(order_product.quantity * products.price) AS cost
            FROM   orders
                   LEFT JOIN order_product
                          ON orders.id = order_product.order_id
                   LEFT JOIN products
                           ON products.id = order_product.product_id
            WHERE customer_id = ?
            GROUP BY id
        ", $id)->fetchAll(PDO::FETCH_CLASS, $this->modelClass);
    }

    public function find($id): Model
    {
        $order = $this->db->query("
            SELECT orders.id,
                   orders.customer_id,
                   customers.name AS customer_name,
                   SUM(order_product.quantity * products.price) AS cost
            FROM   orders
                   LEFT JOIN order_product
                          ON orders.id = order_product.order_id
                   LEFT JOIN products
                          ON products.id = order_product.product_id
                   LEFT JOIN customers
                          ON customers.id = orders.customer_id
            WHERE orders.id = ?
            GROUP BY id
        ", $id)->fetchObject($this->modelClass);

        $order->products = $this->db->query('
            SELECT products.*,
                   quantity,
                   (price * quantity) AS subtotal
            FROM order_product
                 LEFT JOIN products
                        ON products.id = order_product.product_id
            WHERE order_id = ?
        ', $order->id)->fetchAll(PDO::FETCH_CLASS, Product::class);

        return $order;
    }

    public function addProductToOrder(Model $order, string $product, int $quantity)
    {
        return $this->db->query(
            'INSERT INTO order_product VALUES (?, ?, ?)
            ON DUPLICATE KEY UPDATE quantity = quantity + ?',
            $order->id,
            $product,
            $quantity, $quantity
        )->rowCount();
    }

    public function removeProductFromOrder(Model $order, string $product)
    {
        return $this->db->query('
            DELETE FROM order_product
            WHERE order_id = ? AND product_id = ?
            LIMIT 1
        ', $order->id, $product)->rowCount();
    }
}
