<?php

namespace App\Repository;

use App\Product;

class ProductsRepository extends AbstractRepository
{
    protected $table = 'products';
    protected $modelClass = Product::class;
}
