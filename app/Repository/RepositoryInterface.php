<?php

namespace App\Repository;

use stdClass;
use App\Database\ModelInterface as Model;

interface RepositoryInterface
{
    /**
     * @param $id
     *
     * @return Model
     */
    public function find($id): Model;

    /**
     * @param $key
     * @param $value
     *
     * @return Model[]
     */
    public function where($key, $value): array;

    public function countAll(): int;

    /**
     * @return Model[]
     */
    public function all(): array;

    public function save(Model $model): Model;

    public function delete(Model $model): bool;
}
