<?php

namespace App\Support\Container;

use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    protected $definitions = [];

    public function get(string $id)
    {
        if (!$this->has($id)) {
            throw new NotFoundException("$id has not been bound in the container");
        }

        return $this->definitions[$id];
    }

    public function has(string $id): bool
    {
        return array_key_exists($id, $this->definitions);
    }

    public function make(string $id, ...$params)
    {
        $definition = $this->has($id) ? $this->get($id) : $id;

        if (is_object($definition)) {
            return $definition;
        }

        if (!class_exists($definition)) {
            throw new NotFoundException("Class $id does not exist");
        }

        $class = new ReflectionClass($id);
        $constructor = $class->getConstructor();

        if ($constructor === null) {
            return $class->newInstance();
        }

        $argumentIndex = 0;
        $arguments = array_reduce(
            $constructor->getParameters(),
            function (
                array $carry,
                ReflectionParameter $parameter
            ) use ($params, &$argumentIndex) {
                try {
                    if (isset($params[$argumentIndex])) {
                        // The parameter was supplied in the array

                        array_push($carry, $params[$argumentIndex]);

                        return $carry;
                    }

                    // The parameter should be retrieved from the container
                    array_push(
                        $carry,
                        $this->make($parameter->getClass()->getName())
                    );

                    return $carry;
                } finally {
                    ++$argumentIndex;
                }
            }, []
        );

        return $class->newInstanceArgs($arguments);
    }

    public function add(string $id, $value = null): void
    {
        unset($this->definitions[$id]);

        is_null($value) && $value = $id;

        $this->definitions[$id] = $value;
    }
}
