<?php

namespace App\Support\Container;

use Psr\Container\NotFoundExceptionInterface as ExceptionInterface;

class NotFoundException extends ContainerException implements ExceptionInterface
{
    //
}
