<?php

namespace App\Support;

use PDO;

class Database
{
    public $pdo;

    public function __construct(string $host, string $database_name, string $user, string $pass)
    {
        $this->pdo = new PDO("mysql:host=$host;dbname=$database_name", $user, $pass);
    }

    public function query(string $statement, ...$bindings): \PDOStatement
    {
        if (!$bindings) {
            return $this->pdo->query($statement);
        }
        $statement = $this->pdo->prepare($statement);
        $statement->execute($bindings);

        $error = $statement->errorInfo();
        if ($error[1] > 0) {
            throw new \Exception($error[2]);
        }

        return $statement;
    }
}
