<?php

namespace App\Support;

use InvalidArgumentException;

class View
{
    private function __construct() { }

    public static function render(string $name, array $data = [])
    {
        $_viewIncludePath = realpath(__DIR__ . "/../../views/$name.php");

        if (!file_exists($_viewIncludePath)) {
            throw new InvalidArgumentException("View '$name' does not exist");
        }

        if (extract($data, EXTR_SKIP) !== count($data)) {
            throw new InvalidArgumentException(
                "One or more values passed to the view '$name' are reserved"
            );
        }

        return require $_viewIncludePath;
    }
}
