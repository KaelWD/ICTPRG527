<?php

use App\Support\Container\Container;

if (!function_exists('app')) {
    function app(string $id = null)
    {
        static $container;

        if ($container === null) {
            $container = new Container();
        }

        if (isset($id)) {
            return $container->get($id);
        }

        return $container;
    }
}

if (!function_exists('e')) {
    function e(string $string = null): string {
        return htmlspecialchars($string, ENT_QUOTES | ENT_HTML5, 'UTF-8', true);
    }
}
