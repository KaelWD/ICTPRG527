<?php

define('START_TIME', microtime(true));

require_once 'autoload.php';
require_once 'app/Support/helpers.php';

app()->add('config', require 'config.php');

app()->add(
    App\Support\Database::class,
    app()->make(
        App\Support\Database::class,
        app('config')['database']['host'],
        app('config')['database']['name'],
        app('config')['database']['user'],
        app('config')['database']['pass']
    )
);
