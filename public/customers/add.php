<?php

require_once '../../bootstrap.php';

use App\Support\View;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        View::render('customer/create');
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
