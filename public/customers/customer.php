<?php

require_once '../../bootstrap.php';

use App\Customer;
use App\Repository\OrdersRepository;
use App\Support\View;
use App\Repository\CustomersRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $customersRepository = app()->make(CustomersRepository::class);
        $ordersRepository = app()->make(OrdersRepository::class);
        $customer = $customersRepository->find($_GET['id']);
        $customer->orders = $ordersRepository->whereCustomerID($customer->id);

        View::render('customer/edit', compact('customer'));
        break;

    case 'POST':
        $customersRepository = app()->make(CustomersRepository::class);

        if (array_key_exists('id', $_POST)) {
            $customer = $customersRepository->find($_POST['id']);
            $successVerbed = 'edited';
        } else {
            $customer = new Customer();
            $successVerbed = 'created';
        }

        $customer->name = $_POST['name'];
        $customer->address = $_POST['address'];
        $customer->phone = $_POST['phone'];

        $customerID = $customersRepository->save($customer)->id;

        header('refresh:1;url=/customers/');
        View::render(
            'success',
            ['message' => "Customer $successVerbed successfully"]
        );
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
