<?php

require_once '../../bootstrap.php';

use App\Support\View;
use App\Repository\CustomersRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $customersRepository = app()->make(CustomersRepository::class);

        $customers = $customersRepository->allWithOrdersCount();

        View::render('customer/list', compact('customers'));
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
