<?php

require_once '../../bootstrap.php';

use App\Support\View;
use App\Repository\CustomersRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $customersRepository = app()->make(CustomersRepository::class);
        $customer = $customersRepository->find($_POST['id']);

        $orders = app(\App\Support\Database::class)->query('
            SELECT COUNT(*) AS count FROM orders WHERE customer_id = ?
        ', $customer->id)->fetch(\PDO::FETCH_OBJ)->count;

        if ($orders > 0) {
            header("refresh:5;url=/orders/");
            exit("$orders orders exist that belong to this customer. Please update those orders before attempting to delete the customer.");
        }

        $customersRepository->delete($customer);
        header("refresh:2;url=/customers/");
        View::render(
            'success',
            ['message' => "Customer '$customer->name' deleted successfully"]
        );
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
