<?php

require_once '../bootstrap.php';

use App\Support\View;
use App\Repository\ProductsRepository;
use App\Repository\CustomersRepository;
use App\Repository\OrdersRepository;

$productsCount = app()->make(ProductsRepository::class)->countAll();
$customersCount = app()->make(CustomersRepository::class)->countAll();
$ordersCount = app()->make(OrdersRepository::class)->countAll();

View::render('home', compact('productsCount', 'customersCount', 'ordersCount'));
