<?php

require_once '../../bootstrap.php';

use App\Support\View;
use App\Repository\CustomersRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $customers = app()->make(CustomersRepository::class)->all();

        View::render('order/create', compact('customers'));
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
