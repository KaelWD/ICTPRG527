<?php

require_once '../../bootstrap.php';

use App\Support\View;
use App\Repository\OrdersRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $ordersRepository = app()->make(OrdersRepository::class);

        $orders = $ordersRepository->all();

        View::render('order/list', compact('orders'));
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
