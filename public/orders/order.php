<?php

require_once '../../bootstrap.php';

use App\Order;
use App\Support\View;
use App\Repository\OrdersRepository;
use App\Repository\ProductsRepository;
use App\Repository\CustomersRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $ordersRepository = app()->make(OrdersRepository::class);
        $order = $ordersRepository->find($_GET['id']);

        $products = app()->make(ProductsRepository::class)->all();
        $customers = app()->make(CustomersRepository::class)->all();

        View::render('order/edit', compact('order', 'products', 'customers'));
        break;

    case 'POST':
        $ordersRepository = app()->make(OrdersRepository::class);

        if ($_POST['id'] ?? false) {
            $orderID = $_POST['id'];
            $order = $ordersRepository->find($orderID);
        } else {
            $order = new Order();
        }

        switch ($_POST['action'] ?? null) {
            case 'add-product':
                $ordersRepository->addProductToOrder(
                    $order, $_POST['product'], $_POST['quantity']
                );
                break;
            case 'remove-product':
                $ordersRepository->removeProductFromOrder(
                    $order, $_POST['product']
                );
                break;
            default:
                $order->customer_id = $_POST['customer'];
                $orderID = $ordersRepository->save($order)->id;
        }

        header("refresh:0;url=/orders/order.php?id=$orderID");
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
