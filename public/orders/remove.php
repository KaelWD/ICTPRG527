<?php

require_once '../../bootstrap.php';

use App\Support\View;
use App\Repository\OrdersRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $ordersRepository = app()->make(OrdersRepository::class);
        $order = $ordersRepository->find($_POST['id']);
        $ordersRepository->delete($order);
        header("refresh:2;url=/orders/");
        View::render(
            'success',
            ['message' => "Order '$order->id' deleted successfully"]
        );
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
