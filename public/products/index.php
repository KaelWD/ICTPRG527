<?php

require_once '../../bootstrap.php';

use App\Support\View;
use App\Repository\ProductsRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $productsRepository = app()->make(ProductsRepository::class);

        $products = $productsRepository->all();

        View::render('product/list', compact('products'));
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
