<?php

require_once '../../bootstrap.php';

use App\Product;
use App\Support\View;
use App\Repository\ProductsRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $productsRepository = app()->make(ProductsRepository::class);
        $product = $productsRepository->find($_GET['id']);

        View::render('product/edit', compact('product'));
        break;

    case 'POST':
        $productsRepository = app()->make(ProductsRepository::class);

        if (array_key_exists('id', $_POST)) {
            $product = $productsRepository->find($_POST['id']);
            $successVerbed = 'edited';
        } else {
            $product = new Product();
            $successVerbed = 'created';
        }

        $product->name = $_POST['name'];
        $product->description = $_POST['description'];
        $product->price = $_POST['price'] * 100;
        $product->stock = $_POST['stock'] ?: 0;

        $productID = $productsRepository->save($product)->id;

        header('refresh:1;url=/products/');
        View::render(
            'success',
            ['message' => "Product $successVerbed successfully"]
        );
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
