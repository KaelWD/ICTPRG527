<?php

require_once '../../bootstrap.php';

use App\Support\View;
use App\Repository\ProductsRepository;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $productsRepository = app()->make(ProductsRepository::class);
        $product = $productsRepository->find($_POST['id']);

        $orders = app(\App\Support\Database::class)->query('
            SELECT COUNT(*) AS count FROM order_product WHERE product_id = ?
        ', $product->id)->fetch(\PDO::FETCH_OBJ)->count;

        if ($orders > 0) {
            header("refresh:5;url=/orders/");
            exit("$orders orders exist that contain this product. Please update those orders before attempting to delete the product.");
        }

        $productsRepository->delete($product);
        header("refresh:2;url=/products/");
        View::render(
            'success',
            ['message' => "Product '$product->name' deleted successfully"]
        );
        break;

    default:
        throw new HttpException(
            "Invalid request type {$_SERVER['REQUEST_METHOD']}"
        );
}
