CREATE TABLE customers (
    id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    phone VARCHAR(255) NOT NULL
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

CREATE TABLE orders (
    id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    customer_id INT(10) UNSIGNED NOT NULL,
    INDEX orders_customer_id_foreign (customer_id),
    CONSTRAINT orders_customer_id_foreign
        FOREIGN KEY (customer_id) REFERENCES customers (id)
        ON UPDATE CASCADE
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

CREATE TABLE products (
    id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    price INT(10) UNSIGNED NOT NULL DEFAULT 0,
    stock INT(10) UNSIGNED NOT NULL DEFAULT 0
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

CREATE TABLE order_product (
    order_id INT(10) UNSIGNED NOT NULL,
    product_id INT(10) UNSIGNED NOT NULL,
    quantity INT(10) UNSIGNED NOT NULL DEFAULT 1,
    PRIMARY KEY (order_id, product_id),
    INDEX order_product_order_id_foreign (order_id),
    INDEX order_product_product_id_foreign (product_id),
    CONSTRAINT order_product_order_id_foreign
        FOREIGN KEY (order_id) REFERENCES orders (id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT order_product_product_id_foreign
        FOREIGN KEY (product_id) REFERENCES products (id)
        ON UPDATE CASCADE
) COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;
