<?php

/** @var float $startTime */
$startTime = microtime(true);

require 'autoload.php';

// Assertions only throw warnings by default for some reason
ini_set('zend.assertions', 1);
ini_set('assert.exception', 1);

// Get all files in the tests directory
$testCases = scandir('tests');

// Only list files that end in 'Test.php'
$testCases = preg_grep('/Test\.php$/', $testCases);

// Transform file names into fully qualified class names
$testCases = array_map(function (string $name) {
    $name = preg_replace('/\.php$/', '', $name);

    return "Tests\\$name";
}, $testCases);

// Reset the array indices
$testCases = array_values($testCases);

/** @var stdClass[] $results */
$results = null;

$testCasesCount = count($testCases);
echo 'Running ', $testCasesCount, " test case", $testCasesCount === 1 ? '' : 's';
echo ":\n";

/** @var Tests\TestCase[] $testCases */
foreach ($testCases as $testCase) {
    $output = $testCase::run($results);

    // Output the returned status of each test method
    foreach ($output as $string) {
        echo $string;
    }

    // TestCase::run is a Generator, so we'll get
    // the results array for the next test case
    $results = $output->getReturn();
}

$elapsedTime = microtime(true) - $startTime;

// Format elapsed time as a string with appropriate precision
if ($elapsedTime < 0.01) {
    $elapsedTime = round($elapsedTime * 1000, 2) . 'ms';
} elseif ($elapsedTime < 0.1) {
    $elapsedTime = round($elapsedTime * 1000) . 'ms';
} else {
    $elapsedTime = round($elapsedTime, 2) . 's';
}

$totalCount = count($results);

$failed = array_filter($results, function (stdClass $item) {
    return !$item->passed;
});
$failedCount = count($failed);

$passedCount = $totalCount - $failedCount;

$passedPercent = round($passedCount / $totalCount * 100);

echo str_pad(
    "$passedCount / $totalCount ($passedPercent%)",
    80 - ($totalCount % 25),
    ' ',
    STR_PAD_LEFT
), "\n\n";

if ($failedCount === 0) {
    echo "\e[30;42m", str_pad("OK ($totalCount tests) in $elapsedTime", 80), "\n\e[0m";
} else {
    echo "\e[30;41m";
    echo str_pad("FAILED ($totalCount total, " . $failedCount . " failed) in $elapsedTime", 80);
    echo "\n\e[0m\n";

    echo "Failures:\n\n";

    $failureIndex = 0;

    foreach ($failed as $method => $failure) {
        ++$failureIndex;
        echo "$failureIndex) $method\n";
        echo $failure->message, "\n\n";
    }
}
