<?php

namespace Tests;

use App\Support\Container\Container;

class ContainerTest extends TestCase
{
    public function test_it_can_create_objects_with_no_dependencies()
    {
        $container = new Container();

        assert($container instanceof Container, 'Container is created');

        assert(
            $container->make(
                NoDependenciesClass::class
            ) instanceof NoDependenciesClass
        );
    }

    public function test_it_can_create_objects_with_primitive_dependencies()
    {
        $container = new Container();

        $instance = $container->make(PrimitiveDependenciesClass::class, 'foo', 6);

        assert($instance->someString === 'foo');
        assert($instance->someInt === 6);
    }

    public function test_it_can_create_objects_with_autowired_dependencies()
    {
        $container = new Container();

        $instance = $container->make(AutowiredDependenciesClass::class);

        assert($instance->noDependenciesClass instanceof NoDependenciesClass);
    }

    public function test_it_can_create_objects_with_mixed_dependencies()
    {
        $container = new Container();

        /** @var MixedDependenciesClass $instance */
        $instance = $container->make(
            MixedDependenciesClass::class, 'foo', null, 6
        );

        assert($instance->someString === 'foo');
        assert($instance->autoWiredDependenciesClass instanceof AutowiredDependenciesClass);
        assert($instance->someInt === 6);
    }

    public function test_it_can_create_and_share_singletons()
    {
        $container = new Container();

        $instance = $container->make(NoDependenciesClass::class);

        $container->add(NoDependenciesClass::class, $instance);

        assert($container->get(NoDependenciesClass::class) === $instance);
    }

    public function test_it_can_create_singletons_with_dependencies()
    {
        $container = new Container();

        $container->add(
            PrimitiveDependenciesClass::class,
            $container->make(
                PrimitiveDependenciesClass::class,
                'foo',
                6
            )
        );

        $singleton = $container->get(PrimitiveDependenciesClass::class);

        assert($singleton->someString === 'foo');
        assert($singleton->someInt === 6);
    }

    public function test_it_can_create_objects_that_depend_on_singletons()
    {
        $container = new Container();

        $singleton = $container->make(NoDependenciesClass::class);

        $container->add(
            NoDependenciesClass::class,
            $singleton
        );

        $instance = $container->make(AutowiredDependenciesClass::class);

        assert($instance->noDependenciesClass === $singleton);
    }

    public function test_it_can_store_non_class_values()
    {
        $container = new Container();

        $config = [
            'database' => [
                'user' => 'foo',
                'pass' => 'bar',
            ],
        ];

        $container->add('config', $config);

        assert($container->get('config') === $config);
    }
}

class NoDependenciesClass
{
    public function __construct() { }
}

class PrimitiveDependenciesClass
{
    public $someString;
    public $someInt;

    public function __construct(string $someString, int $someInt)
    {
        $this->someString = $someString;
        $this->someInt = $someInt;
    }
}

class AutowiredDependenciesClass
{
    public $noDependenciesClass;

    public function __construct(NoDependenciesClass $noDependenciesClass)
    {
        $this->noDependenciesClass = $noDependenciesClass;
    }
}

class MixedDependenciesClass
{
    public $someString;
    public $autoWiredDependenciesClass;
    public $someInt;

    public function __construct(
        string $somestring,
        AutowiredDependenciesClass $autowiredDependenciesClass,
        int $someInt
    )
    {
        $this->someString = $somestring;
        $this->autoWiredDependenciesClass = $autowiredDependenciesClass;
        $this->someInt = $someInt;
    }
}
