<?php

namespace Tests;

use Generator;
use Throwable;
use AssertionError;
use ReflectionClass;
use ReflectionMethod;

abstract class TestCase
{
    public static function run(?array $results): Generator
    {
        $case = new static;
        $testMethods = static::getTestMethods();

        foreach ($testMethods as $test) {
            $methodName = "{$test->class}::{$test->getName()}";
            try {
                call_user_func([$case, $test->getName()]);
                $results[$methodName] = (object) [
                    'passed' => true,
                ];
            } catch (AssertionError $err) {
                $results[$methodName] = (object) [
                    'passed'  => false,
                    'message' => "Assertion failed on line {$err->getLine()}: {$err->getMessage()}",
                ];
            } catch (Throwable $err) {
                $results[$methodName] = (object) [
                    'passed'  => false,
                    'message' => $err->getMessage(),
                ];
            }

            // Limit the line length to 25
            if (count($results) % 25 === 0) {
                yield "\n";
            }

            if ($results[$methodName]->passed) {
                yield '.';
            } else {
                yield "\e[31mF\e[0m";
            }
        }

        return $results;
    }

    /**
     * @return ReflectionMethod[]
     */
    protected static function getTestMethods(): array
    {
        $class = new ReflectionClass(static::class);

        $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);

        // Filter to test methods only
        $methods = array_filter(
            $methods,
            function (ReflectionMethod $method): bool {
                return preg_match('/^test/', $method->getName());
            }
        );

        // Reset the array keys
        return array_values($methods);
    }
}
