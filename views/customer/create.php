<?php

use App\Support\View;

View::render('partials/head', ['title' => 'Add Customer']);
?>

    <div class="container py-3">
        <h1 class="mb-3">Add Customer</h1>

        <nav class="breadcrumb">
            <a href="/" class="breadcrumb-item">Dashboard</a>
            <a href="/customers/" class="breadcrumb-item">Customers</a>
            <a href="/customers/add.php" class="breadcrumb-item active">Add customer</a>
        </nav>

<?php View::render(
        'partials/customer-form',
        ['customer' => new \App\Customer()]
); ?>

    </div>

<?php
View::render('partials/foot');
