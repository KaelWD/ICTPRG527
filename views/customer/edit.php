<?php

use App\Support\View;

/** @var \App\Customer $customer */

View::render('partials/head', ['title' => "Edit customer $customer->id"]);
?>

    <div class="container py-3">
        <h1 class="mb-3">Edit Customer <?= $customer->id ?></h1>

        <nav class="breadcrumb">
            <a href="/" class="breadcrumb-item">Dashboard</a>
            <a href="/customers/" class="breadcrumb-item">Customers</a>
            <a href="/customers/customer.php?id=<?= $customer->id ?>"
               class="breadcrumb-item active">Edit customer</a>
        </nav>

        <?php View::render('partials/customer-form', compact('customer')); ?>

        <hr>

        <h3 class="mb-3">Orders</h3>

        <table class="table table-hover table-bordered">
            <thead class="thead-default">
            <tr>
                <th>ID</th>
                <th>Items</th>
                <th>Cost</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($customer->orders as $order) { ?>
                <tr>
                    <td>
                        <a href="/orders/order.php?id=<?= $order->id ?>">
                            <?= $order->id ?>
                        </a>
                    </td>
                    <td><?= $order->itemsCount ?? 0 ?></td>
                    <td><?= $order->costString() ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>

<?php
View::render('partials/foot');
