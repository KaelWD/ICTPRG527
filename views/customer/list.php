<?php

use App\Support\View;

/** @var App\Customer[] $customers */

View::render('partials/head', ['title' => 'Customers']);
?>

<div class="container py-3">
    <h1 class="mb-3">Customers</h1>

    <nav class="breadcrumb">
        <a href="/" class="breadcrumb-item">Dashboard</a>
        <a href="/customers/" class="breadcrumb-item active">Customers</a>
    </nav>

    <a href="/customers/add.php" class="btn btn-success mb-3 float-right">+ Add</a>

    <table class="table table-hover table-bordered">
        <thead class="thead-default">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>Phone number</th>
            <th>Orders</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($customers as $customer) { ?>
            <tr>
                <td>
                    <a href="/customers/customer.php?id=<?= $customer->id ?>">
                        <?= $customer->id ?>
                    </a>
                </td>
                <td>
                    <a href="/customers/customer.php?id=<?= $customer->id ?>">
                        <?= e($customer->name) ?>
                    </a>
                </td>
                <td><?= e($customer->address) ?></td>
                <td><?= e($customer->phone) ?></td>
                <td><?= $customer->ordersCount ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<?php

View::render('partials/foot');
