<?php

use App\Support\View;

/**
 * @var int $productsCount
 * @var int $customersCount
 * @var int $ordersCount
 */

View::render('partials/head', ['title' => 'Dashboard']);
?>

<div class="container py-3">
    <h1 class="mb-3">Dashboard</h1>

    <div class="d-flex flex-row flex-wrap">
        <a class="card no-underline mr-3" href="/products/">
            <div class="card-body">
                <p class="display-1" style="min-width: 4ch">
                    <?= $productsCount ?>
                </p>
                <p class="text-muted mb-0">
                    Product<?= $productsCount === 1 ? '' : 's' ?>
                </p>
            </div>
        </a>

        <a class="card no-underline mr-3" href="/customers/">
            <div class="card-body">
                <p class="display-1" style="min-width: 4ch">
                    <?= $customersCount ?>
                </p>
                <p class="text-muted mb-0">
                    Customer<?= $customersCount === 1 ? '' : 's' ?>
                </p>
            </div>
        </a>

        <a class="card no-underline mr-3" href="/orders/">
            <div class="card-body">
                <p class="display-1" style="min-width: 4ch">
                    <?= $ordersCount ?>
                </p>
                <p class="text-muted mb-0">
                    Order<?= $ordersCount === 1 ? '' : 's' ?>
                </p>
            </div>
        </a>
    </div>
</div>

<?php

View::render('partials/foot');
