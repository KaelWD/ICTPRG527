<?php

use App\Support\View;

/** @var \App\Customer[] $customers */

View::render('partials/head', ['title' => 'Add Order']);
?>

    <div class="container py-3">
        <h1 class="mb-3">Add Order</h1>

        <nav class="breadcrumb">
            <a href="/" class="breadcrumb-item">Dashboard</a>
            <a href="/orders/" class="breadcrumb-item">Orders</a>
            <a href="/orders/add.php" class="breadcrumb-item active">Add order</a>
        </nav>

        <form action="/orders/order.php" method="post">

            <div class="form-group row">
                <label for="orderID" class="col-sm-2 col-form-label">
                    ID
                </label>
                <div class="col-sm-10">
                    <input id="orderID"
                           name="id"
                           type="text"
                           class="form-control"
                           value=""
                           disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="customer" class="col-sm-2 col-form-label">
                    Customer
                </label>
                <div class="col-sm-10">
                    <select id="customer" name="customer" class="form-control custom-select">
                        <option value="" disabled>ID: Name</option>
                        <option value="" selected disabled>
                            ---------------------------
                        </option>
                        <?php foreach ($customers as $customer) { ?>
                            <option value="<?= $customer->id ?>">
                                <?= $customer->id ?>: <?= e($customer->name) ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>

        </form>

    </div>

<?php
View::render('partials/foot');
