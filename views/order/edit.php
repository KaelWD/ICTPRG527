<?php

use App\Support\View;

/**
 * @var \App\Order      $order
 * @var \App\Customer[] $customers
 * @var \App\Product[]  $products
 */

View::render('partials/head', ['title' => 'Edit order']);
?>

<div class="container py-3">
    <h1 class="mb-3">Edit order <?= $order->id ?></h1>

    <nav class="breadcrumb">
        <a href="/" class="breadcrumb-item">Dashboard</a>
        <a href="/orders/" class="breadcrumb-item">Orders</a>
        <a href="/orders/order.php?id=<?= $order->id ?>" class="breadcrumb-item active">Edit order</a>
    </nav>

    <form action="/orders/order.php" method="post">

        <div class="form-group row">
            <label for="orderID" class="col-sm-2 col-form-label">
                ID
            </label>
            <div class="col-sm-10">
                <input id="orderID"
                       name="id"
                       type="text"
                       class="form-control"
                       value="<?= $order->id ?>"
                       disabled>
            </div>
        </div>

        <div class="form-group row">
            <label for="customer" class="col-sm-2 col-form-label">
                Customer
            </label>
            <div class="col-sm-10">
                <select id="customer" name="customer" class="form-control custom-select">
                    <option value="" disabled>ID: Name</option>
                    <option value="" disabled>
                        ---------------------------
                    </option>
                    <?php foreach ($customers as $customer) { ?>
                        <option value="<?= $customer->id ?>"
                            <?= $customer->id === $order->customer_id ?
                                'selected' : '' ?>
                        >
                            <?= $customer->id ?>: <?= e($customer->name) ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Save</button>

        <button type="button"
                class="btn btn-danger float-right"
                data-toggle="modal"
                data-target="#deleteConfirmation">
            Delete
        </button>
    </form>

    <hr>

    <h3 class="mb-3">Products</h3>

    <form action="/orders/order.php" method="post" class="form-inline mb-3">
        <input type="hidden" name="action" value="add-product">
        <input type="hidden" name="id" value="<?= $order->id ?>">

        <label class="mr-2" for="product">Add product:</label>
        <select name="product" id="product" class="form-control custom-select mr-5" style="flex-grow: 1">
            <option value="" selected disabled>-------------------------</option>
            <?php foreach ($products as $product) { ?>
                <option value="<?= $product->id ?>">
                    <?= $product->id ?>: <?= e($product->name) ?> (<?= $product->priceString() ?>)
                </option>
            <?php } ?>
        </select>
        <label for="quantity" class="mr-2">Quantity:</label>
        <input type="number" name="quantity" id="quantity" class="form-control mr-2" step="1" min="1" value="1">

        <button type="submit" class="btn btn-success">+</button>
    </form>

    <table class="table table-hover table-bordered">
        <thead class="thead-default">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Unit price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($order->products as $product) { ?>
            <tr>
                <td>
                    <a href="/products/product.php?id=<?= $product->id ?>">
                        <?= $product->id ?>
                    </a>
                </td>
                <td>
                    <a href="/products/product.php?id=<?= $product->id ?>">
                        <?= e($product->name) ?>
                    </a>
                </td>
                <td>
                    <a href="/products/product.php?id=<?= $product->id ?>">
                        <?= e($product->description) ?>
                    </a>
                </td>
                <td><?= $product->priceString() ?></td>
                <td><?= $product->quantity ?></td>
                <td><?= $product->priceString($product->subtotal) ?></td>
                <td>
                    <form action="/orders/order.php" method="post" class="form-inline">
                        <input type="hidden" name="action" value="remove-product">
                        <input type="hidden" name="id" value="<?= $order->id ?>">
                        <input type="hidden"
                               name="product"
                               value="<?= $product->id ?>"
                        >
                        <button type="submit" class="btn btn-danger mx-auto">X</button>
                    </form>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <div class="display-4">
        Total: <span class="float-right"><?= $order->costString() ?></span>
    </div>

    <div class="modal fade" id="deleteConfirmation" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content bg-danger text-white">
                <div class="modal-header">
                    <h5 class="modal-title">Are you sure?</h5>
                    <button type="button"
                            class="close text-white"
                            data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete order
                        '<?= $order->id ?>'?
                    </p>
                    <p class="mb-0">THIS ACTION CANNOT BE UNDONE</p>
                </div>
                <form class="modal-footer"
                      action="/orders/remove.php"
                      method="post">
                    <input type="hidden"
                           name="id"
                           value="<?= $order->id ?>"
                           hidden>
                    <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-warning">
                        Yes, I'm sure
                    </button>
                </form>
            </div>
        </div>
    </div>

</div>

<?php

View::render('partials/foot');
