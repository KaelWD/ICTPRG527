<?php

use App\Support\View;

/** @var App\Order[] $orders */

View::render('partials/head', ['title' => 'Orders']);
?>

<div class="container py-3">
    <h1 class="mb-3">Orders</h1>

    <nav class="breadcrumb">
        <a href="/" class="breadcrumb-item">Dashboard</a>
        <a href="/orders/" class="breadcrumb-item active">Orders</a>
    </nav>

    <a href="/orders/add.php"
       class="btn btn-success mb-3 float-right">+ Add</a>

    <table class="table table-hover table-bordered">
        <thead class="thead-default">
        <tr>
            <th>Order ID</th>
            <th>Customer ID</th>
            <th>Customer Name</th>
            <th>Items</th>
            <th>Cost</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($orders as $order) { ?>
            <tr>
                <td>
                    <a href="/orders/order.php?id=<?= $order->id ?>">
                        <?= $order->id ?>
                    </a>
                </td>
                <td>
                    <a href="/customers/customer.php?id=<?= $order->customer_id ?>">
                        <?= $order->customer_id ?>
                    </a>
                </td>
                <td>
                    <a href="/customers/customer.php?id=<?= $order->customer_id ?>">
                        <?= e($order->customer_name) ?>
                    </a>
                </td>
                <td><?= $order->itemsCount ?? 0 ?></td>
                <td><?= $order->costString() ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<?php

View::render('partials/foot');
