<?php

/** @var \App\Customer $customer */

$customerExists = (bool) $customer->id;
?>

<form action="/customers/customer.php" method="post">

    <div class="form-group row">
        <label for="customerID" class="col-sm-2 col-form-label">
            ID
        </label>
        <div class="col-sm-10">
            <input id="customerID"
                   name="id"
                   type="text"
                   class="form-control"
                   value="<?= $customer->id ?>"
                   <?= $customerExists ? 'readonly' : 'disabled' ?>
            >
        </div>
    </div>

    <div class="form-group row">
        <label for="customerName" class="col-sm-2 col-form-label">
            Name
        </label>
        <div class="col-sm-10">
            <input id="customerName"
                   name="name"
                   type="text"
                   class="form-control"
                   value="<?= e($customer->name) ?>"
            >
        </div>
    </div>

    <div class="form-group row">
        <label for="customerAddress" class="col-sm-2 col-form-label">
            Address
        </label>
        <div class="col-sm-10">
            <input id="customerAddress"
                   name="address"
                   type="text"
                   class="form-control"
                   value="<?= e($customer->address) ?>"
            >
        </div>
    </div>

    <div class="form-group row">
        <label for="customerPhone" class="col-sm-2 col-form-label">
            Phone number
        </label>
        <div class="col-sm-10">
            <input id="customerPhone"
                   name="phone"
                   type="tel"
                   class="form-control"
                   value="<?= e($customer->phone) ?>"
            >
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Save</button>

    <?php if ($customerExists) { ?>
        <button type="button"
                class="btn btn-danger float-right"
                data-toggle="modal"
                data-target="#deleteConfirmation">
            Delete
        </button>
    <?php } ?>
</form>

<?php if ($customerExists) { ?>
    <div class="modal fade" id="deleteConfirmation" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content bg-danger text-white">
                <div class="modal-header">
                    <h5 class="modal-title">Are you sure?</h5>
                    <button type="button"
                            class="close text-white"
                            data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete customer
                        '<?= e($customer->name) ?>'?
                    </p>
                    <p class="mb-0">THIS ACTION CANNOT BE UNDONE</p>
                </div>
                <form class="modal-footer"
                      action="/customers/remove.php"
                      method="post">
                    <input type="hidden"
                           name="id"
                           value="<?= $customer->id ?>"
                           hidden>
                    <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-warning">
                        Yes, I'm sure
                    </button>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
