<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>

<?php
$elapsedTime = microtime(true) - START_TIME;

// Format elapsed time as a string with appropriate precision
if ($elapsedTime < 0.01) {
    $elapsedTime = round($elapsedTime * 1000, 2) . 'ms';
} elseif ($elapsedTime < 0.1) {
    $elapsedTime = round($elapsedTime * 1000) . 'ms';
} else {
    $elapsedTime = round($elapsedTime, 2) . 's';
}
?>

<div class="container">
    <small class="float-right">Generated in <?= $elapsedTime ?></small>
</div>

</body>
</html>
