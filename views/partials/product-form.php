<?php

/** @var \App\Product $product */

$productExists = (bool) $product->id;
?>

<form action="/products/product.php" method="post">

    <div class="form-group row">
        <label for="productID" class="col-sm-2 col-form-label">ID</label>
        <div class="col-sm-10">
            <input type="text"
                   id="productID"
                   name="id"
                   class="form-control"
                   value="<?= $product->id ?>"
                   <?= $productExists ? 'readonly' : 'disabled' ?>
            >
        </div>
    </div>

    <div class="form-group row">
        <label for="productName" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="text"
                   id="productName"
                   name="name"
                   class="form-control"
                   value="<?= e($product->name) ?>"
            >
        </div>
    </div>

    <div class="form-group row">
        <label for="productDescription" class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <input type="text"
                   id="productDescription"
                   name="description"
                   class="form-control"
                   value="<?= e($product->description) ?>"
            >
        </div>
    </div>

    <div class="form-group row">
        <label for="productPrice" class="col-sm-2 col-form-label">Price</label>
        <div class="input-group col-sm-10">
            <div class="input-group-addon">$</div>
            <input type="number"
                   id="productPrice"
                   name="price"
                   class="form-control"
                   step="0.01"
                   value="<?= number_format($product->price / 100, 2) ?>"
            >
        </div>
    </div>

    <div class="form-group row">
        <label for="productStock" class="col-sm-2 col-form-label">Stock</label>
        <div class="col-sm-10">
            <input type="number"
                   id="productStock"
                   name="stock"
                   class="form-control"
                   step="1"
                   value="<?= $product->stock ?>"
            >
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Save</button>

    <?php if ($productExists) { ?>
        <button type="button"
                class="btn btn-danger float-right"
                data-toggle="modal"
                data-target="#deleteConfirmation">
            Delete
        </button>
    <?php } ?>
</form>

<?php if ($productExists) { ?>
    <div class="modal fade" id="deleteConfirmation" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content bg-danger text-white">
                <div class="modal-header">
                    <h5 class="modal-title">Are you sure?</h5>
                    <button type="button"
                            class="close text-white"
                            data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete product
                        '<?= e($product->name) ?>'?
                    </p>
                    <p class="mb-0">THIS ACTION CANNOT BE UNDONE</p>
                </div>
                <form class="modal-footer"
                      action="/products/remove.php"
                      method="post">
                    <input type="hidden"
                           name="id"
                           value="<?= $product->id ?>"
                           hidden>
                    <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-warning">
                        Yes, I'm sure
                    </button>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
