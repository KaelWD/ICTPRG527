<?php

use App\Support\View;

View::render('partials/head', ['title' => 'Add Product']);
?>

    <div class="container py-3">
        <h1 class="mb-3">Add Product</h1>

        <nav class="breadcrumb">
            <a href="/" class="breadcrumb-item">Dashboard</a>
            <a href="/products/" class="breadcrumb-item">Products</a>
            <a href="/products/add.php" class="breadcrumb-item active">Add product</a>
        </nav>

        <?php View::render(
            'partials/product-form',
            ['product' => new \App\Product()]
        ); ?>

    </div>

<?php
View::render('partials/foot');
