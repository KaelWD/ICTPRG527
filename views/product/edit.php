<?php

use App\Support\View;

/** @var \App\Product $product */

View::render('partials/head', ['title' => "Edit product $product->id"]);
?>

    <div class="container py-3">
        <h1 class="mb-3">Edit Product <?= $product->id ?></h1>

        <nav class="breadcrumb">
            <a href="/" class="breadcrumb-item">Dashboard</a>
            <a href="/products/" class="breadcrumb-item">Products</a>
            <a href="/products/product.php?id=<?= $product->id ?>"
               class="breadcrumb-item">Edit product</a>
        </nav>

        <?php View::render('partials/product-form', compact('product')); ?>

    </div>

<?php
View::render('partials/foot');
