<?php

use App\Support\View;

/** @var App\Product[] $products */

View::render('partials/head', ['title' => 'Products']);
?>

    <div class="container py-3">
        <h1 class="mb-3">Products</h1>

        <nav class="breadcrumb">
            <a href="/" class="breadcrumb-item">Dashboard</a>
            <a href="/products/" class="breadcrumb-item active">Products</a>
        </nav>

        <a href="/products/add.php"
           class="btn btn-success mb-3 float-right">+ Add</a>

        <table class="table table-hover table-bordered">
            <thead class="thead-default">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Stock</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product) { ?>
                <tr>
                    <td>
                        <a href="/products/product.php?id=<?= $product->id ?>">
                            <?= $product->id ?>
                        </a>
                    </td>

                    <td>
                        <a href="/products/product.php?id=<?= $product->id ?>">
                            <?= e($product->name) ?>
                        </a>
                    </td>

                    <td><?= e($product->description) ?></td>

                    <td><?= $product->priceString() ?></td>

                    <td><?= $product->stock ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

<?php

View::render('partials/foot');
