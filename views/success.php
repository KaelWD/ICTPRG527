<?php

use App\Support\View;

/** @var string $message */

View::render('partials/head', ['title' => 'Success!'])
?>

<div class="container py-3">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Success!</h4>
        <p><?= e($message) ?></p>
        <hr>
        <p class="mb-0">
            <small>You will be redirected automatically, please wait</small>
        </p>
    </div>
</div>

<?php

View::render('partials/foot');
